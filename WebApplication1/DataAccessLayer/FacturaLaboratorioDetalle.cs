//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class FacturaLaboratorioDetalle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FacturaLaboratorioDetalle()
        {
            this.AnalisisResultadoes = new HashSet<AnalisisResultado>();
        }
    
        public int FacturaLaboratorioDetalleID { get; set; }
        public int FacturaLaboratorioID { get; set; }
        public int LaboratorioAnalisisID { get; set; }
        public decimal Cantidad { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal Descuento { get; set; }
        public decimal Precio { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnalisisResultado> AnalisisResultadoes { get; set; }
        public virtual FacturaLaboratorio FacturaLaboratorio { get; set; }
        public virtual LaboratorioAnalisi LaboratorioAnalisi { get; set; }
    }
}
