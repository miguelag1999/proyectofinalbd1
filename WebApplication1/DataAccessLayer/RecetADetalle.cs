//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class RecetADetalle
    {
        public int ReceteDetalleID { get; set; }
        public int RecetaID { get; set; }
        public int MedicamentoID { get; set; }
        public decimal Cantidad { get; set; }
    
        public virtual Medicamento Medicamento { get; set; }
        public virtual Receta Receta { get; set; }
    }
}
