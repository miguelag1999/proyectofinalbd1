﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class FarmaciaMedicamentoModel
    {
        public int FarmaciaMedicamentoID { get; set; }
        public int FarmaciaID { get; set; }
        public FarmaciaModel Farmacia { get; set; }
        public int MedicamentoID { get; set; }
        public MedicamentoModel Medicamento { get; set; }
        public DateTime FechaDisponibilidad { get; set; }
        public decimal CantidadDisponible { get; set; }
        public decimal Precio { get; set; }
    }
}
