﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class PersonaModel
    {
        public int PersonaID { get; set; }
        public string Nombre { get; set; }
        public int TipoPersonaID { get; set; }
        public int Edad { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Identificacion { get; set; }
        public string TipoSangre { get; set; }
        public int SeguroID { get; set; }
        public SeguroModel Seguro { get; set; }
        public int EspecialidadID { get; set; }
        public EspecialidadModel Especialidad { get; set; }
        public string NoLicencia { get; set; }

    }
}
