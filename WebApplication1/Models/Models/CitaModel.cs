﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class CitaModel
    {
        public int CitaID { get; set; }
        public int DoctorID { get; set; }
        public PersonaModel Doctor { get; set; }
        public int PacienteID { get; set; }
        public PersonaModel Paciente { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Hora { get; set; }
        public byte Estatus { get; set; }

    }
}
