﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class AdmisionModel
    {
        public int AdmisionID { get; set; }
        public int VisitaMedicaID { get; set; }
        public VisitaMedicaModel VisitaMedica { get; set; }
        public int HabitacionID { get; set; }
        public HabitacionModel Habitacion { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        public byte Estatus { get; set; }
    }
}
