﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class AnalisisModel
    {
        public int AnalisisID { get; set; }
        public string Descripcion { get; set; }
        public int EdadMinimaRequerida { get; set; }
        public int TiempoEstimado { get; set; }
        public decimal RangoMinimo { get; set; }
        public decimal RangoMaximo { get; set; }
    }
}
