﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class FacturaFarmaciaDetalleModel
    {
        public int FacturaFarmaciaDetalleID { get; set; }
        public int FacturaFarmaciaID { get; set; }
        public FacturaFarmaciaModel FacturaFarmacia { get; set; }
        public int FarmaciaMedicamentoID { get; set; }
        public FarmaciaMedicamentoModel FarmaciaMedicamento { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Descuento { get; set; }
        public decimal Precio { get; set; }
    }
}
