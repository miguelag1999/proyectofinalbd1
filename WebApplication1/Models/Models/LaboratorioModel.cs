﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class LaboratorioModel
    {
        public int LaboratorioID { get; set; }
        public string Descripcion { get; set; }
        public string Direccion { get; set; }
        public string RNC { get; set; }
        public string Telefono { get; set; }


    }
}
