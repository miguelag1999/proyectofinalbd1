﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class LaboratorioAnalisisModel
    {
        public int LaboratorioAnalisisID { get; set; }
        public int LaboratorioID { get; set; }
        public LaboratorioModel Laboratorio { get; set; }
        public int AnalisisID { get; set; }
        public AnalisisModel AnalisisID { get; set; }
        public decimal Precio { get; set; }
    }
}
