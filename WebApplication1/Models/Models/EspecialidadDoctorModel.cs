﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class EspecialidadDoctorModel
    {
        public int EspecialidadDoctorID { get; set; }
        public int DoctorID { get; set; }
        public PersonaModel Doctor { get; set; }
        public int EspecialidadID { get; set; }
        public EspecialidadModel Especialidad { get; set; }

    }
}
