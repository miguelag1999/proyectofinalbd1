﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class RecetaModel
    {
        public int RecetaID { get; set; }
        public int VisitaMedicaID { get; set; }
        public VisitaMedicaModel VisitaMedica { get; set; }
        public DateTime FechaValidez { get; set; }
        public DateTime FechaEmision { get; set; }
    }
}
