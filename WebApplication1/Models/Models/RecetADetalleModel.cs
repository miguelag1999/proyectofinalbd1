﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class RecetADetalleModel
    {
        public int ReceteDetalleID { get; set; }
        public int RecetaID { get; set; }
        public RecetaModel Receta { get; set; }
        public int MedicamentoID { get; set; }
        public MedicamentoModel Medicamento { get; set; }
        public decimal Cantidad { get; set; }
    }
}
