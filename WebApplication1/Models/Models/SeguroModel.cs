﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class SeguroModel
    {
        public int SeguroID { get; set; }
        public string NoSeguro { get; set; }
        public string Aseguradora { get; set; }

    }
}
