﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class DiagnosticoModel
    {
        public int DiagnosticoID { get; set; }
        public int VisitaMedicaID { get; set; }
        public VisitaMedicaModel VisitaMedica { get; set; }
        public string Comentario { get; set; }
    }
}
