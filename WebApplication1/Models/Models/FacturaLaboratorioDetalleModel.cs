﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class FacturaLaboratorioDetalleModel
    {
        public int FacturaLaboratorioDetalleID { get; set; }
        public int FacturaLaboratorioID { get; set; }
        public FacturaLaboratorioModel FacturaLaboratorio { get; set; }
        public int LaboratorioAnalisisID { get; set; }
        public LaboratorioAnalisisModel LaboratorioAnalisis { get; set; }
        public decimal Cantidad { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal Descuento { get; set; }
        public decimal Precio { get; set; }
    }
}
