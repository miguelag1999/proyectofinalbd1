﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class LicenciaMedicaModel
    {
        public int LicenciaMedicaID { get; set; }
        public int ConsultaID { get; set; }
        public VisitaMedicaModel Consulta { get; set; }
        public string Causa { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
    }
}
