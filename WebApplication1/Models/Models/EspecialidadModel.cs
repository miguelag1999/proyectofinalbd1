﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    class EspecialidadModel
    {
        public int EspecialidadID { get; set; }
        public string Descripcion { get; set; }
        public int SubEspecialidadID { get; set; }
        public EspecialidadModel SubEspecialidad { get; set; }

    }
}
