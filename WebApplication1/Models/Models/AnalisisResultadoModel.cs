﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class AnalisisResultadoModel
    {
        public int AnalisisResultadoID { get; set; }
        public int FacturaLaboratorioDetalleID { get; set; }
        public FacturaLaboratorioDetalleModel FacturaLaboratorioDetalle { get; set; }
        public decimal Resultado { get; set; }
        public byte Estatus { get; set; }
    }
}
