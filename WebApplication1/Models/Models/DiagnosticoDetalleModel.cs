﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class DiagnosticoDetalleModel
    {
        public int DiagnosticoDetalleID { get; set; }
        public int DiagnosticoID { get; set; }
        public DiagnosticoModel Diagnostico { get; set; }
        public string Code { get; set; }
    }
}
