﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class FacturaFarmaciaModel
    {
        public int FacturaFarmaciaID { get; set; }
        public int RecetaID { get; set; }
        public RecetaModel Receta { get; set; }
        public DateTime Fecha { get; set; }
        public decimal SubTotal { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal Descuentos { get; set; }
        public decimal MontoTotal { get; set; }
    }
}
