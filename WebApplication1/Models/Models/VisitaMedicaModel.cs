﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class VisitaMedicaModel
    {
        public int VisitaMedicaID { get; set; }
        public int HospitalID { get; set; }
        public HospitalModel Hospital { get; set; }
        public int PacienteID { get; set; }
        public PersonaModel Paciente { get; set; }
        public int DoctorID { get; set; }
        public PersonaModel Doctor { get; set; }
        public DateTime Fecha { get; set; }
        public int Tipo { get; set; }
        public string Motivo { get; set; }
        public int CitaID { get; set; }
        public CitaModel Cita { get; set; }
        public int Prioridad { get; set; }
    }
}
