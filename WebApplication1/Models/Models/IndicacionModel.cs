﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class IndicacionModel
    {
        public int IndicacionID { get; set; }
        public int VisitaMedicaID { get; set; }
        public VisitaMedicaModel VisitaMedica { get; set; }
        public DateTime FechaValidez { get; set; }
        public DateTime FechaAdmision { get; set; }
    }
}
