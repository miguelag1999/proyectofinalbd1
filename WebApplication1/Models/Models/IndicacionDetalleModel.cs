﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class IndicacionDetalleModel
    {
        public int IndicacionDetalleID { get; set; }
        public int IndicacionID { get; set; }
        public IndicacionModel Indicacion { get; set; }
        public int AnalisisID { get; set; }
        public AnalisisModel Analisis { get; set; }
    }
}
