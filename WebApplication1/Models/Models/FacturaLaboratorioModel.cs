﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    class FacturaLaboratorioModel
    {
        public int FacturaLaboratorioID { get; set; }
        public int IndicacionID { get; set; }
        public IndicacionModel Indicacion { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Subtotal { get; set; }
        public decimal MontoImpuestos { get; set; }
        public decimal Descuentos { get; set; }
        public decimal MontoTotal { get; set; }
    }
}
